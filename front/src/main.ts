import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import io from 'socket.io-client';

createApp(App).use(router).mount('#app')


const socket = io('https://3000-omnibusek-webapp-h36gx7secvz.ws-eu44.gitpod.io/', {
    reconnection: false,
    transports: ["websocket", "polling"]
});

